#Auto SSH

Auto SSH is a collection of automatically ssh login scripts.

#Python-ssh.py

python-ssh.py is a python scripts which run commands on a remote server through ssh.

##Python-ssh.py Usage

    python-ssh.py <user@host> [password] <commands>

The [password] is an optional parameter. Make sure you can ssh with your private ssh key at .ssh/id_rsa.
